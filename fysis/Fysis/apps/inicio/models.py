from django.db import models
from django.template.defaultfilters import slugify


class Partida(models.Model):
    pin = models.CharField(primary_key = True, max_length = 15)
    fase = models.CharField(max_length = 30)
    resultado = models.CharField(max_length = 30)
    estado = models.BooleanField(blank=True)

    def save(self, *args, **kwargs):
        super(Partida, self).save(*args, **kwargs)
    
    def __str__(self):
        return (self.pin)

class Rol(models.Model):
    idRol = models.CharField(primary_key = True, max_length = 15)
    nombre = models.CharField(max_length = 15)

    def save(self, *args, **kwargs):

        super(Rol, self).save(*args, **kwargs)

    def __str__(self):
        return (self.nombre)

class Jugador(models.Model):
    #El id es el generado automáticamente por Django 
    nick = models.CharField(max_length = 15)
    estado = models.BooleanField()
    rol = models.ForeignKey('Rol')
    partida = models.ForeignKey('Partida')

    def save(self, *args, **kwargs):

        super(Jugador, self).save(*args, **kwargs)

    def __str__(self):
        return (self.nick)

     # Llama al "verdadero" método save().