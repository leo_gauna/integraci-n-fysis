from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from .models import Partida
from .models import Jugador
from .models import Rol
from .forms import form_jugador
import random

def inicio(request):
	pin = random.randint(1,1000)
	while Partida.objects.filter(pin=pin).exists():
			pin = random.randint(1,1000)
	new_pin = Partida.objects.create(pin=pin, fase='dia', resultado='jugando', estado=True)

	ctx={}
	ctx['formato_jugador'] = form_jugador()
	ctx['pin'] = pin
	return render(request, 'inicio/inicio.html',ctx)

def conectarUsuario(request):
	roles = Rol.objects.get(idRol= '1')
	nuevo_jugador = form_jugador(request.POST)
	if nuevo_jugador.is_valid() :

		if Jugador.objects.filter(partida=nuevo_jugador.cleaned_data['pin']).count() <= 10: #Pregunta si la cantidad de jugadores de una partida es menor o igual a 10

			
				partida = Partida.objects.get(pin=nuevo_jugador.cleaned_data['pin'])
				Jugador.objects.create(nick=nuevo_jugador.cleaned_data['nick'], estado=True, rol=roles, partida=partida)
				return redirect('partida:esperarPartida')
			
		else:
			ctx = {}
			ctx['informe'] = 'PARTIDA LLENA'
			return render(request, 'inicio/inicio.html', ctx)
	else:
		ctx={}
		ctx['formato_jugador'] = nuevo_jugador
		return render(request, 'inicio/inicio.html', ctx)

def ultimousuario(request):
	if request . method == 'POST' :
		data = 'Prueba'

	return HttpResponse ( json.dumps ( data ))