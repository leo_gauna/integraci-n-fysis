from django.conf.urls import url, include 
from .views import inicio, conectarUsuario, ultimousuario


urlpatterns = [
	url(r'^$', inicio, name='inicio'),
	url(r'^unirse/', conectarUsuario, name='conectarUsuario'),
	url(r'^lleno/', ultimousuario, name='ultimousuario'),

]