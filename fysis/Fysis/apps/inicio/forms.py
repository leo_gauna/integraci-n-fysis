from django import forms
from .models import Jugador

class form_jugador(forms.ModelForm):
	pin = forms.CharField(max_length = 15)
	class Meta:
		model = Jugador
		fields = ['nick',]